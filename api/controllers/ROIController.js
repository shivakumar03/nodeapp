' use strict';


var mongoose = require('mongoose'),
ROIModel = mongoose.model('ROIModel');

exports.list_all_entries = function(req, res) {
    ROIModel.find({}, function(err, model) {
    if (err)
      res.send(err);
    res.json(model);
  });
};

exports.create_an_entry = function(req, res) {
  var new_entry = new ROIModel(req.body);
  new_entry.save(function(err, new_entry) {
    if (err)
      res.send(err);
    res.json(new_entry);
  });
};
