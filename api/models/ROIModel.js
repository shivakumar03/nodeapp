'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ROISchema = new Schema({
  BuyAmount: {
    type: Number,
  },
  BuyStage: {
    type: Number,
  },
  HoldPeriod: {
    type: Number,
  },
  TotalValue: {
    type: Number,
  },
  ReturnsObtained: {
    type: Number,
  },
  TotalAssets: {
    type: Number,
  },
});

module.exports = mongoose.model('ROIModel', ROISchema);