'use strict';
module.exports = function(app) {
  var ROI = require('../controllers/ROIController');

  // todoList Routes
  app.route('/ROI')
    .get(ROI.list_all_entries)
    .post(ROI.create_an_entry);


};