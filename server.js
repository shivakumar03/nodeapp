var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  ROIModel = require('./api/models/ROIModel'), //created model loading here
  bodyParser = require('body-parser'),
  url = require('url'),
  nodemailer = require("nodemailer");

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/TempDb'); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/ROIRoutes'); //importing route
routes(app); //register the route

app.post('/sendMail', function (req, res) {
    var q = url.parse(req.url, true).query;
    var mailId = q.mailId;
    //send mail
    var data;
    ROIModel.find({}, function(err, model) {
        if (err)
          res.send(err);
        // res.json(model);
        data = model;
        var tableHtml = '<table style="border:1px solid #ddd;"><th style="border:1px solid #ddd;">Buy Amount</th><th style="border:1px solid #ddd;">Buy Stage</th><th style="border:1px solid #ddd;">Hold Period</th><th style="border:1px solid #ddd;">Total Value</th><th style="border:1px solid #ddd;">Returns Obtained</th><th style="border:1px solid #ddd;">Total Assets</th>';
        data.forEach(function(item){
            console.log(item);
            tableHtml = tableHtml+'<tr>';
            tableHtml = tableHtml+'<td style="border:1px solid #ddd;">'+item.BuyAmount+'</td>';
            tableHtml = tableHtml+'<td style="border:1px solid #ddd;">'+item.BuyStage+'</td>';
            tableHtml = tableHtml+'<td style="border:1px solid #ddd;">'+item.HoldPeriod+'</td>';
            tableHtml = tableHtml+'<td style="border:1px solid #ddd;">'+item.TotalValue+'</td>';
            tableHtml = tableHtml+'<td style="border:1px solid #ddd;">'+item.ReturnsObtained+'</td>';            
            tableHtml = tableHtml+'<td style="border:1px solid #ddd;">'+item.TotalAssets+'</td>';            
            tableHtml = tableHtml+'</tr>';
        });
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: 'shivakumarbegari03@gmail.com',
              pass: '**********'
            }
        });
         
        var mailOptions = {
            from: 'shivakumarbegari03@gmail.com',
            to: mailId,
            subject: 'Return on Investment graph',
            html: tableHtml,
          };
          
          transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });
        res.json(data);
    });
})

app.listen(port);


console.log('todo list RESTful API server started on: ' + port);
